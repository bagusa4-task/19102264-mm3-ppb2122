package com.bas_19102264.praktikum10

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Quote(
    var id: Int = 0,
    var title: String? = null,
    var description: String? = null,
    var category: String? = null,
    var date: String? = null,
    var author: String? = null,
    var url: String? = null
) : Parcelable