package com.bas_19102264.praktikum6.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bas_19102264.praktikum6.DetailActivity
import com.bas_19102264.praktikum6.MyData
import com.bas_19102264.praktikum6.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CardViewMyDataAdapter(private val listMyDatas: ArrayList<MyData>, val context: Context):
    RecyclerView.Adapter<CardViewMyDataAdapter.CardViewViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CardViewViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
        return CardViewViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardViewViewHolder, position: Int) {
        holder.bind(listMyDatas[position], context)
    }
    override fun getItemCount(): Int = listMyDatas.size

    class CardViewViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgPhoto: ImageView
        val tvName: TextView
        val tvDetail: TextView
        val btnFavorite: Button
        val btnShare: Button

        init {
            imgPhoto = itemView.findViewById(R.id.img_item_photo)
            tvName = itemView.findViewById(R.id.tv_item_name)
            tvDetail = itemView.findViewById(R.id.tv_item_detail)
            btnFavorite = itemView.findViewById(R.id.btn_set_favorite)
            btnShare = itemView.findViewById(R.id.btn_set_share)

        }

        fun bind(myData: MyData, context: Context) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(myData.photo)
                    .apply(RequestOptions().override(350, 550))
                    .into(imgPhoto)
            }
            tvName.text = myData.name
            tvDetail.text = myData.description
            btnFavorite.setOnClickListener { Toast.makeText(itemView.context, "Favorite " + myData.name, Toast.LENGTH_SHORT).show() }
            btnShare.setOnClickListener { Toast.makeText(itemView.context, "Share " + myData.name, Toast.LENGTH_SHORT).show() }
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Kamu memilih " + myData.name, Toast.LENGTH_SHORT).show()
                val moveWithObjectIntent = Intent(context, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MYDATA, myData)
                context.startActivity(moveWithObjectIntent)
            }
        }
    }
}