package com.bas_19102264.praktikum6.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bas_19102264.praktikum6.DetailActivity
import com.bas_19102264.praktikum6.MyData
import com.bas_19102264.praktikum6.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class GridMyDataAdapter(private val listMyData: ArrayList<MyData>, val context: Context):
    RecyclerView.Adapter<GridMyDataAdapter.GridViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): GridViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_grid, viewGroup, false)
        return GridViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.bind(listMyData[position], context)
    }
    override fun getItemCount(): Int = listMyData.size

    class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img_item_photo: ImageView

        init {
            img_item_photo = itemView.findViewById(R.id.img_item_photo)
        }

        fun bind(myData: MyData, context: Context) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(myData.photo)
                    .apply(RequestOptions().override(350, 550))
                    .into(img_item_photo)
            }
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Kamu memilih " + myData.name, Toast.LENGTH_SHORT).show()
                val moveWithObjectIntent = Intent(context, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MYDATA, myData)
                context.startActivity(moveWithObjectIntent)
            }
        }
    }
}