package com.bas_19102264.praktikum6.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bas_19102264.praktikum6.DetailActivity
import com.bas_19102264.praktikum6.MyData
import com.bas_19102264.praktikum6.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ListMyDataAdapter(private val listMyData: ArrayList<MyData>, val context: Context):
    RecyclerView.Adapter<ListMyDataAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listMyData[position], context)
    }
    override fun getItemCount(): Int = listMyData.size

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img_item_photo: ImageView
        val tv_item_name: TextView
        val tv_item_description: TextView

        init {
            img_item_photo = itemView.findViewById(R.id.img_item_photo)
            tv_item_name = itemView.findViewById(R.id.tv_item_name)
            tv_item_description = itemView.findViewById(R.id.tv_item_description)
        }

        fun bind(myData: MyData, context: Context) {
            with(itemView){
                Glide.with(itemView.context)
                    .load(myData.photo)
                    .apply(RequestOptions().override(55, 55))
                    .into(img_item_photo)
                tv_item_name.text = myData.name
                tv_item_description.text = myData.description
            }
            itemView.setOnClickListener {
                Toast.makeText(itemView.context, "Kamu memilih " + myData.name, Toast.LENGTH_SHORT).show()
                val moveWithObjectIntent = Intent(context, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MYDATA, myData)
                context.startActivity(moveWithObjectIntent)
            }
        }
    }
}