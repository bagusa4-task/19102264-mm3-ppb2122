package com.bas_19102264.praktikum6

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.TextView
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.bas_19102264.praktikum6.databinding.ActivityDetailBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val myData by getParcelableExtra<MyData>(DetailActivity.EXTRA_MYDATA)

        setSupportActionBar(findViewById(R.id.toolbar))
//        binding.toolbarLayout.title = title
//        supportActionBar?.title = myData?.name.toString()
        binding.toolbarLayout.title = myData?.name.toString()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Would you like to open Maps?", Snackbar.LENGTH_LONG)
                .setAction("Open") {
                    val moveWithObjectIntent = Intent(this, MapsActivity::class.java)
                    moveWithObjectIntent.putExtra(MapsActivity.EXTRA_MYDATA, myData)
                    startActivity(moveWithObjectIntent)
                }.show()
        }

        binding.contentScrolling.tvDetailDescription.text = myData?.description.toString()
        Glide.with(this)
            .load(myData?.photo.toString())
            .apply(RequestOptions().override(700, 700))
            .into(binding.ivDetailPhoto)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        const val EXTRA_MYDATA = "extra_mydata"
    }
    inline fun <reified T : Parcelable> Activity.getParcelableExtra(key: String) = lazy {
        intent.getParcelableExtra<T>(key)
    }
}