package com.bas_19102264.praktikum14.model

data class QuoteResponse(
    val quotes: ArrayList<Quote>)