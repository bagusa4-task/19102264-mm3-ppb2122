package com.bas_19102264.praktikum14.ui.myquotes

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bas_19102264.praktikum14.QuoteAddUpdateActivity
import com.bas_19102264.praktikum14.R
import com.bas_19102264.praktikum14.TokenPref
import com.bas_19102264.praktikum14.adapter.QuoteAdapter
import com.bas_19102264.praktikum14.api.MainPresenter
import com.bas_19102264.praktikum14.databinding.FragmentMyQuotesBinding
import com.bas_19102264.praktikum14.helper.REQUEST_ADD
import com.bas_19102264.praktikum14.interfaces.CoroutineContextProvider
import com.bas_19102264.praktikum14.interfaces.MainView
import com.bas_19102264.praktikum14.model.Login
import com.bas_19102264.praktikum14.model.Quote
import com.bas_19102264.praktikum14.model.Token
import com.bas_19102264.praktikum14.ui.classquotes.DashboardViewModel

class MyQuotesFragment : Fragment(), MainView {
    private lateinit var presenter: MainPresenter
    private var quotes: MutableList<Quote> = mutableListOf()
    private lateinit var adapter: QuoteAdapter
    private lateinit var tokenPref: TokenPref
    private lateinit var token: Token
    private var _binding: FragmentMyQuotesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMyQuotesBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentMyQuotesBinding.bind(view)
        binding.recyclerviewMyQuotes.layoutManager = LinearLayoutManager(activity)

        tokenPref = TokenPref(requireActivity())
        token = tokenPref.getToken()
        adapter = QuoteAdapter(requireActivity())

        binding.recyclerviewMyQuotes.adapter = adapter
        presenter = MainPresenter(this, CoroutineContextProvider())
        binding.progressbar.visibility = View.VISIBLE

        presenter.getMyQuotes(token.token)
        binding.swiperefresh.setOnRefreshListener {
            binding.progressbar.visibility = View.INVISIBLE
            presenter.getMyQuotes(token.token)
        }

        binding.fab.setOnClickListener {
            val intent = Intent(requireActivity(), QuoteAddUpdateActivity::class.java)
            startActivityForResult(intent, REQUEST_ADD)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getMyQuotes(token.token)
    }
    override fun showMessage(messsage: String) {
        Toast.makeText(requireActivity(),messsage, Toast.LENGTH_SHORT).show()
    }
    override fun resultQuote(data: ArrayList<Quote>) {
        quotes.clear()
        adapter.listQuotes = data
        quotes.addAll(data)
        adapter.notifyDataSetChanged()
        binding.progressbar.visibility = View.INVISIBLE
        binding.swiperefresh.isRefreshing = false
    }
    override fun resultLogin(data: Login) {
    }
}

//class MyQuotesFragment : Fragment() {
//
//    private var _binding: FragmentNotificationsBinding? = null
//
//    // This property is only valid between onCreateView and
//    // onDestroyView.
//    private val binding get() = _binding!!
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View {
//        val notificationsViewModel =
//            ViewModelProvider(this).get(NotificationsViewModel::class.java)
//
//        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
//        val root: View = binding.root
//
//        val textView: TextView = binding.textNotifications
//        notificationsViewModel.text.observe(viewLifecycleOwner) {
//            textView.text = it
//        }
//        return root
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        _binding = null
//    }
//}

