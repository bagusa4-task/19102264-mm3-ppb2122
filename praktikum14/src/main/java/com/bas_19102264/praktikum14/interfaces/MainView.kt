package com.bas_19102264.praktikum14.interfaces

import com.bas_19102264.praktikum14.model.Login
import com.bas_19102264.praktikum14.model.Quote

interface MainView {
    fun showMessage(messsage : String)
    fun resultQuote(data: ArrayList<Quote>)
    fun resultLogin(data: Login)
}